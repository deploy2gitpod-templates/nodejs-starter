# fluffy-train
Generate a new VM inside GitPod using this blank template, useful if you want to create an status page like what GitPod does.

## How to use this template?
Open this repository in GitPod using the button below.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io#https://github.com/RecapTime/fluffy-train)

After that, change the `origin` repository into something else in the new console session.
```bash
git remote set origin <new-git-url>
```

Done? Enjoy editing stuff on your side.

## Who created this blank template?
[Andrei Jiroh.](https://github.com/AndreiJirohHaliliDev2006)
